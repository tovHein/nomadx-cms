$(document).ready(function() {
  calculateDuration();
  calculateImages();

  $('.dragimage').draggable({
    containment: '.playlist-container',
    cursor: 'move',
    helper: addNewImage,       
    revert: "invalid",
  });

  $('.dragvideo').draggable({
    containment: '.playlist-container',
    cursor: 'move',
    helper: addNewVideo,       
    revert: "invalid",
  });

    //------ sortable rows --------------
    $(".schedule-content").sortable({
    	cancel: ".fixed, input",
    });
    $(".schedule-content").disableSelection();
    // ----------------------------------


    $('.schedule-content.video').droppable({
     accept: ".dragvideo, .dragimage",
     drop: function (ev, ui) {
       ui.helper.attr('style', '');
       $item = $(ui.helper).clone();
       var dataType = ui.helper.data('type');
       if( dataType == "image"){
         ui.helper.append('<div><i class="fa fa-clock-o" aria-hidden="true"></i>  Duration: <span class="badge badge-image" data-duration="10"> <input type="text" name="setTime" value="50"/>s </span></div><a onclick="deleteVidimage(this)" class="clone-delete"> <i class="fa fa-times" aria-hidden="true"></i> </a>');
       }
       ui.helper.clone().appendTo($(this));
       calculateDuration();
     },
   });

    $('.schedule-content.image').droppable({
     accept: ".dragimage",
     drop: function (ev, ui) {
       ui.helper.attr('style', '');
       ui.helper.append('<div><i class="fa fa-clock-o" aria-hidden="true"></i>  Duration: <span class="badge badge-image" data-duration="10"> <input type="text" name="setTime" value="50"/>s </span></div><a onclick="deletePimage(this)" class="clone-delete"> <i class="fa fa-times" aria-hidden="true"></i> </a>');
       ui.helper.clone().appendTo($(this));
       calculateImages();
     },
   });

    $('.schedule-content.logo').droppable({
     accept: ".dragimage",
     drop: function (ev, ui) {
       ui.helper.attr('style', '');
       ui.helper.append('<a onclick="deletePimage(this)" class="clone-delete"> <i class="fa fa-times" aria-hidden="true"></i> </a>');
       ui.helper.clone().appendTo($(this));
       calculateImages();
     },
   });

  });

function calculateDuration(){
  var sum = 0;
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $('.current-screen .tab-pane.video.active .schedule-content.video .badge').each(function(){
      sum += parseInt( $(this).data('duration'));
    });
    var minutes = Math.floor(sum / 60);
    var seconds = sum - minutes * 60;
    $('.total-vtime .time').html( minutes + ' min ' + seconds + ' sec' );
  })
  $('.current-screen .tab-pane.video.active .schedule-content.video .badge').each(function(){
    sum += parseInt( $(this).data('duration'));
  });
  var minutes = Math.floor(sum / 60);
  var seconds = sum - minutes * 60;
  $('.total-vtime .time').html( minutes + ' min ' + seconds + ' sec' );
}

function calculateImages(){
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var totalImage = $('.tab-pane.active .schedule-content.image .clone, .tab-pane.active .schedule-content.logo .clone').length;
    $('#schedule-create-playlist .totalImage').html( totalImage );
  })
  var totalImage = $('.tab-pane.active .schedule-content.image .clone, .tab-pane.active .schedule-content.logo .clone').length;
  $('#schedule-create-playlist .totalImage').html( totalImage );
}

function addNewImage() {
  var clone = $(this).html();
  return '<div class="clone" data-type="image">' + clone + '</div>';
}
function deletePimage(that){
  $(that).parent().parent().droppable({
    accept: ".dragimage, .dragvideo",
    drop: function (ev, ui) {
      ui.helper.attr('style', '');
      ui.helper.append('<a onclick="deletePimage(this)" class="clone-delete"> <i class="fa fa-times" aria-hidden="true"></i> </a>');
      ui.helper.clone().appendTo($(this));
      $('.tab-pane.active .schedule-content.logo .clone').find('div').remove();
      calculateImages();
    },
  });
  $(that).parent().remove();
  calculateImages();
  calculateDuration();
}
function deleteVidimage(that){
  $(that).parent().parent().droppable({
    accept: ".dragimage, .dragvideo",
    drop: function (ev, ui) {
      ui.helper.attr('style', '');
      var dataType = ui.helper.data('type');
      if( dataType == "image"){
       ui.helper.append('<div><i class="fa fa-clock-o" aria-hidden="true"></i>  Duration: <span class="badge badge-image" data-duration="10"> <input type="text" name="setTime" value="50"/>s </span></div><a onclick="deleteVidimage(this)" class="clone-delete"> <i class="fa fa-times" aria-hidden="true"></i> </a>');
     }
     ui.helper.clone().appendTo($(this));
     $('.tab-pane.active .schedule-content.logo .clone').find('div').remove();
     calculateImages();
   },
 });
  $(that).parent().remove();
  calculateImages();
  calculateDuration();
}
function addNewVideo() {
  var clone = $(this).html();
  return '<div class="clone">' + clone + '<div><i class="fa fa-clock-o" aria-hidden="true"></i>  Duration: <span class="badge bg-primary" data-duration="50"> 50s </span></div><a onclick="deletePvideo(this)" class="clone-delete"> <i class="fa fa-times" aria-hidden="true"></i> </a></div>';
}
function deletePvideo(that){
  $(that).parent().parent().droppable({
    accept: ".dragvideo, .dragimage",
    drop: function (ev, ui) {
      ui.helper.attr('style', '');
      var dataType = ui.helper.data('type');
      if( dataType == "image"){
       ui.helper.append('<div><i class="fa fa-clock-o" aria-hidden="true"></i>  Duration: <span class="badge badge-image" data-duration="10"> <input type="text" name="setTime" value="50"/>s </span></div><a onclick="deleteVidimage(this)" class="clone-delete"> <i class="fa fa-times" aria-hidden="true"></i> </a>');
     }
     ui.helper.clone().appendTo($(this));
     calculateDuration();
   },
 });
  $(that).parent().remove();
  calculateDuration();
}

function addNewThemeRow() {
  var clone = $(this).html();
  return '<div class="clone theme-clone"> ' + clone + '<div><i class="fa fa-clock-o" aria-hidden="true"></i>  Duration: <span class="badge bg-primary" data-duration="50"> 50s </span></div><a onclick="deleteThemevideo(this)" class="clone-delete"> <i class="fa fa-times" aria-hidden="true"></i> </a></div>';
}
function deleteThemevideo(that){
  $(that).parent().parent().droppable({
    accept: ".theme-single-list",
    drop: function (ev, ui) {
      ui.helper.attr('style', '');
      $(ui.helper).addClass('buttonDraggable');
      $(this).droppable("destroy");
      ui.helper.clone().appendTo($(this));
    },
  });
  $(that).parent().remove();
}