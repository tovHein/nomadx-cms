//시작과 동시에 로딩화면 보여주는 부분
$(window).load(function() {
	setTimeout(function() {
		$('#loading').hide();
	}, 10);
});

// <START> Upload Files function in Upload Media Page
function uploadMedia(that) {
	$('#select-screen').slideToggle();
	$('.upload-btn').hide();
}

function cancelMedia(that) {
	$('#select-screen').slideToggle();
	$('.upload-btn').show();
}

$('.screen-box.normal .radio label').click(function() {
	$('.screen-box').removeClass('active').fadeOut();
	$(this).parent().parent().parent().addClass('active');
	$(this).parent().parent().parent().find('.row').fadeIn();
	$('.block-separator').hide();
	$('.select-description').hide();
	$('.file-upload-wrapper.normal').fadeIn().delay(1000);
});

$('.screen-box.theme .radio label').click(function() {
	$('.screen-box').removeClass('active').fadeOut();
	$(this).parent().parent().parent().addClass('active');
	$(this).parent().parent().parent().find('.row').fadeIn();
	$('.block-separator').hide();
	$('.select-description').hide();
	$('.file-upload-wrapper.theme, .theme-template-wrapper').fadeIn().delay(1000);
});

$('#theme-file-upload .panel-heading .nav > li > a').click(function() {
	var target = $(this).data('image');
	$('.theme-template-wrapper img').removeClass('active').hide();
	$(target).addClass('active');
	if (target == '.sea_wind-template') {
		$('#theme').show();
		$('#tribe, #theme2').hide();
	} else if (target == '.mountain_forest-template') {
		$('#theme2').show();
		$('#tribe, #theme').hide();
	} else {
		$('#theme, #theme2').hide();
		$('#tribe').show();
	}
});

function cancle_screen(that) {
	$('.screen-box, .block-separator').addClass('active').fadeIn(1000);
	$('.file-upload-wrapper, .screen-box .row, .theme-template-wrapper').hide();
}

function confirm_screen(that) {
	$('.screen-box, .block-separator').addClass('active').fadeIn(1000);
	$('.file-upload-wrapper, .screen-box .row, .theme-template-wrapper').hide();
}
// <END> Upload Files function in Upload Media Page

// <START> Advance Search function in Upload Media Page
$('.adv-search-btn').click(function() {
	if (!$(this).hasClass('active')) {
		$('.advsearch-content').slideDown();
		$(this).addClass('active');
	} else {
		$(this).removeClass('active');
		$('.advsearch-content').slideUp();
	}
});
// <END> Advance Search function in Upload Media Page

// <START> Image Delete Checkbox Check
$('#result input').on('ifChecked', function(event) {
	$('.delete-selected, .action-selected').fadeIn();
});
$('#result input').on('ifUnchecked', function(event) {
	if ($('#result').find('.icheckbox_square-red.checked').size() <= 1) {
		$('.delete-selected, .action-selected').fadeOut();
	}
});

// Remove the checked state from "All" if any checkbox is unchecked
$('#select-all').on('ifUnchecked', function(event) {
	$('#result').iCheck('uncheck');
});

// Make "All" checked if all checkboxes are checked
$('#select-all').on('ifChecked', function(event) {
	if ($('.check').filter(':checked').length == $('.check').length) {
		$('#result').iCheck('check');
	}
});

function delete_selected(that) {
	$('#result').find('.icheckbox_square-red.checked').parent().parent().parent().parent().remove();
}
// <END> Image Delete Checkbox Check

// <START> Product Popover
function single_delete_yes(that) {
	$(that).parent().parent().parent().parent().remove();
	$('.delete-file').popover('hide');
}

function single_delete_no(that) {
	$('.delete-file').popover('hide');
}
// <END> Product Popover

// <START> edit popup for Media Libiary

$('#setPeriod').on('ifChecked', function (event){
	$('input[name="datetimes"]').show();
});
$('#setPeriod').on('ifUnchecked', function (event) {
	$('input[name="datetimes"]').hide();
});

$('.edit-file').click(function() {
	if ($(this).hasClass('video-edit')) {
		$('#popup').removeClass('image-popup').addClass('video-popup');
		$('#for-video').show();
		$('#for-image').hide()
	} else {
		$('#popup').removeClass('video-popup').addClass('image-popup');
		$('#for-image').show();
		$('#for-video').hide();
	}
	$('#popup').addClass('active');
});

$('input[name="datetimes"]').daterangepicker({
	timePicker: true,
	startDate: moment().startOf('hour'),
	endDate: moment().startOf('hour').add(32, 'hour'),
	locale: {
		format: 'DD/M/YYYY hh:mm A'
	}
});

$('.close-edit-popup').click(function() {
	$('#popup').removeClass('active');
});
// <END> edit popup for Media Libiary

//<START> Image & Video Popup Function
$('.popup-image').magnificPopup({
	type: 'image',
	mainClass: 'mfp-with-zoom',
	zoom: {
		enabled: true,
		duration: 300,
		easing: 'ease-in-out',
		opener: function(openerElement) {
			return openerElement.is('img') ? openerElement : openerElement.find('img');
		}
	}
});

$('.popup-video').magnificPopup({
	disableOn: 700,
	type: 'iframe',
	mainClass: 'mfp-fade',
	removalDelay: 160,
	preloader: false,
	fixedContentPos: false
});
//<END> Image & Video Popup Function


// <START> Schedule Page JS
$('#schedule-select-screen .screen-box .radio label').click(function() {
	$('.screen-box, .select-screen').removeClass('active current').hide();
	var step_title = $(this).data('title');
	$('#schedule-select-screen .screen-box .radio label').removeClass('active');
	$(this).addClass('active');
	if( ($(this).data('target') == "landscape") || ($(this).data('target') == "portrait") ){
		$('.adv-search-btn,#schedule-create-playlist-theme').removeClass('current-screen').hide();
		$('#schedule-create-playlist').addClass('current-screen').fadeIn();
	}else{
		$('#schedule-create-playlist').removeClass('current-screen').hide();
		$('#schedule-create-playlist-theme').addClass('current-screen').fadeIn();
	}
	$('.step-title').html('<span> (' + step_title + ')</span> ');
	$('.actions, .back-select-screen').fadeIn();
	calculateDuration();
});

function selectTheme(){
	$('.theme-list').slideToggle();
}

function cancelCreateGroup(){
	$('#list-theme-group, .theme-media').show();
	$('#create-theme-group, .theme-list, .theme-timeline-container').hide();
}

$('.theme-list div').on('click', function(){
	var target = '.' + $(this).data('theme');
	var title = $(this).data('title');
	$('.mainPreviewWrapper').css( 'background-image', "url('" + $(this).data('bgurl') + "')" ) ;
	$('.mainPreviewWrapper').attr( 'id', $(this).data('theme') );

	$('.theme-group-add').removeClass('currentTheme');
	$(target).addClass('currentTheme');
	
	$('#list-theme-group, .theme-group-add, .theme-timeline-container').hide();
	$('#create-theme-group, .theme-media').show();
	$('.theme-title').html(title);
	$(target).fadeIn();
});

$('.back-select-screen').on('click',function(){
	$('.select-screen, .adv-search-btn').fadeIn();
	$('.screen-box, .block-separator').removeClass('current').addClass('active').fadeIn();
	$('#schedule-create-playlist, #schedule-create-playlist-theme, .back-select-screen').fadeOut();
	$('.theme-timeline-container').hide();
	$('.theme-media').show();
});

function viewTimeline(){
	$('.theme-timeline-container').show();
	$('.theme-media').hide();
}


$('.todo-remove').on('click',function(){
	$(this).parent().parent().remove();
});
// <END> Schedule Page JS

// <START> Overall JS
$('.square-green input').iCheck({
	checkboxClass: 'icheckbox_square-green',
	radioClass: 'iradio_square-green',
        increaseArea: '20%' // optional
    });

$('.square-red input').iCheck({
	checkboxClass: 'icheckbox_square-red',
	radioClass: 'iradio_square-red',
        increaseArea: '20%' // optional
    });

//<START> General Bootstrap Functions
$('[data-toggle="popover"]').popover({
	html: true,
});

$('#theme-accordion').collapse()
//<END> General Bootstrap Functions


// <START> Device Edit
function deviceEdit(){
	$('#popup').addClass('active');
}

$('#show-date-advance').click(function(){
	$('#switch-components').slideToggle();
	$('#operation-hour').slideToggle();
	if( !$(this).hasClass('active')){
		$(this).addClass('active');
		$(this).html('Hide Operation Hours  <i class="fa fa-chevron-down" aria-hidden="true"></i>' );
	}else{
		$(this).removeClass('active');
		$(this).html('Show Operation Hours  <i class="fa fa-chevron-up" aria-hidden="true"></i>' );     
	}
});

//  <END> Device Edit






// <Start> Call bootstrap third party jquery

$('#switch-components .checkbox input').on('ifChecked', function(event){
	$(this).closest('.clearfix').find('label.control-label, .select-time').hide();
});

$('#switch-components .checkbox input').on('ifUnchecked', function(event){
	$(this).closest('.clearfix').find('label.control-label, .select-time').show();
});

$('#id_sch_time3').on('ifChecked', function(event){
	$('#id_sch_time3').val("Y");
	$('.select-time').fadeOut();
	$('#operation-hour #id_time_label1').fadeOut();
	$('#operation-hour #id_time_label2').fadeOut();
	$('#show-date-advance').fadeOut();
});

$('#id_sch_time3').on('ifUnchecked', function(event){         
	$('#operation-hour #id_time_label1').fadeIn();
	$('#operation-hour #id_time_label2').fadeIn();
	$('.select-time').fadeIn();
	$('#show-date-advance').fadeIn();
});

//timepicker start
$('.timepicker-default').timepicker();
//timepicker end

// <END> Call bootstrap third party jquery

