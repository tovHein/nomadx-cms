							let i = 0;
							let x = 0;
							//Number of Logo show in one loop
							let showImages = 28;
							let imageCounter = 0

							let bottomCounter = 0;
							let topCounter = 0;

							let videoSources = [];
							let leftimageSources = [];
							let bottomimageSources = [];
							let topImageSources = [];

							let leftimageSourcesInterval;
							let bottomimageSourcesInterval;
							let topimageSourcesInterval;
							let reCheck;

							function resetValue(){
								i = 0;
								x = 0;
								//Number of Logo show in one loop
								showImages = 28;
								imageCounter = 0

								bottomCounter = 0;
								topCounter = 0;

								videoSources = [];
								leftimageSources = [];
								bottomimageSources = [];
								topImageSources = [];

								clearInterval(leftimageSourcesInterval);
								clearInterval(bottomimageSourcesInterval);
								clearInterval(topimageSourcesInterval);
								clearInterval(reCheck);
								$('#videoPlayer').get(0).src = null;
							}

							function closePreview(val){
								var target = '#' + val;
								$(target).modal('hide');
								resetValue();
							}

							// ========================
							// START for portrait Preivew
							// ========================
							function previewPortraitPopup(){
								$('#previewPortraitPopup').modal('show');
								$('#previewPortraitPopup').addClass( $('#schedule-select-screen label.active').data('target') );
								$('.mainPreviewWrapper').css('background-image','none');
								$('#previewPortraitPopup #portraitImageList').css( 'background-image', "url('" + $('#schedule-select-screen label.active').data('previewbg') + "')" ) ;
								portraitImage();
							}

							function portraitImage(){
								$('#previewPortraitPopup #portraitImageList').show();
								$('#previewPortraitPopup #previewPortraitVideo').hide();
								$("#schedule-create-playlist #logo td .popup-image").each(function() {  
									imgsrc = this.href;
										// ADD to array
										leftimageSources.push( imgsrc );
									});  

								showLogoCalculation(28);

								leftimageSourcesInterval = setInterval( function() {
									showLogoCalculation(28);
								}, 5000 );
							}

							function showLogoCalculation(n){
								var contents = '';
								console.log( imageCounter );
								while ( imageCounter < showImages ) {	
									if( imageCounter < leftimageSources.length ){
										contents += `<img src='${ leftimageSources[imageCounter] }'`;
									}

									if( ( (imageCounter+1) % n ) == 0){
										console.log('show');
										$('#previewPortraitPopup #portraitImageList').empty().append(contents);
									}
									imageCounter++;
								}

								if( showImages <= leftimageSources.length ){
									showImages += n;
									contents='';
								}else{
									clearInterval(leftimageSourcesInterval);
									if(contents == ''){ 
										console.log('null');
										portraitPlaylist();
									}else{
										$('#previewPortraitPopup #portraitImageList').empty().append(contents);
										setTimeout(portraitPlaylist, 5000);
									}
								}
							}

							function portraitPlaylist(){
								i = 0;
								$('#previewPortraitPopup #portraitImageList').hide();
								$('#previewPortraitPopup #previewPortraitVideo').fadeIn();
								$("#schedule-create-playlist .video .popup-video, #schedule-create-playlist .video .popup-image").each(function() {  
									vidsrc = this.href;
										// ADD to array
										videoSources.push( vidsrc );
									}); 

								var filename = videoSources[i++ % videoSources.length];

								checkFile(filename);

								$('#videoportraitPlayer').on('ended',function(){
									this.src = videoSources[i++ % videoSources.length];
									this.load();
									this.play();
									filename = videoSources[i++ % videoSources.length];
									checkFile(filename);
								});
							}

							function portraitNext(){
								var nextNo = i ;
								if( nextNo >= videoSources.length ){
									i = 0;
								}
								var filename = videoSources[i++ % videoSources.length];
								checkFile(filename);
							}

							function portraitPrev(){
								var prevNo = i-2 ;
								if( prevNo < 0 ){
									i = videoSources.length - 1;
								}else{
									i = prevNo;
								}
								var filename = videoSources[i++ % videoSources.length];
								checkFile(filename);
							}

							function checkFile(val){
								var fileext1 = ".jpg";
								var fileext2 = ".png";

								clearInterval(reCheck);
								
								if( val.indexOf(fileext2) != -1  || val.indexOf(fileext1) != -1 ){
									$('#videoportraitPlayer').get(0).src = null;
									$('#previewPortraitVideo #imageVideo').attr('src', val);
									reCheck = setInterval( function() {
										var filename = videoSources[i++ % videoSources.length];
										checkFile(filename);
									}, 5000 );

								}else{
									$('#previewPortraitVideo #imageVideo').attr('src', '');
									$('#videoportraitPlayer').get(0).src = val;
									$('#videoportraitPlayer').get(0).play();
								}
							}

							// ========================
							// END for portrait Preivew
							// ========================

							// Preview Popup for Video Wall
							function previewPopup(){
								$('#previewPopup').modal('show');
								if( $('.theme-group-add.mountain_forest_theme').hasClass('currentTheme') ){
									forestMountainImage();
									playlist(0);
								}else if( $('.theme-group-add.sea_wind_theme').hasClass('currentTheme') ){
									swleftImage();
									swbottomImage();
									swtopImage();
									playlist(0);
								}else if( $('.theme-group-add.intro_video').hasClass('currentTheme') || $('.theme-group-add.intro_video').hasClass('currentTheme') ){
									playlist(0);
								}else{
									leftImage();
									bottomImage();
									topImage();
									playlist(0);
								}
							}


							// Start Video Preview
							function playlist(n){
								i = n;

								$(".theme-group-add.currentTheme .popup-video").each(function() {  
									vidsrc = this.href;
										// ADD to array
										videoSources.push( vidsrc );
									}); 

								$('#videoPlayer').get(0).src = videoSources[i++ % videoSources.length];

								$('#videoPlayer').get(0).play();
								$('#videoPlayer').on('ended',function(){
									this.src = videoSources[i++ % videoSources.length];
									this.load();
									this.play();
								});
							}

							// Intro Video Preview
							function introplaylist(n){
								i = n;

								$(".theme-group-add.currentTheme .popup-video").each(function() {  
									vidsrc = this.href;
										// ADD to array
										videoSources.push( vidsrc );
									}); 

								$('#fwvideoPlayer').get(0).src = videoSources[i++ % videoSources.length];

								$('#fwvideoPlayer').get(0).play();
								$('#fwvideoPlayer').on('ended',function(){
									this.src = videoSources[i++ % videoSources.length];
									this.load();
									this.play();
								});
							}

							// Buttons Functions
							function next(){
								var nextNo = i ;
								if( nextNo >= videoSources.length ){
									i = 0;
								}
								$('#videoPlayer').get(0).src = videoSources[i++ % videoSources.length];
								$('#videoPlayer').get(0).load();
								$('#videoPlayer').get(0).play();
							}

							function prev(){
								var prevNo = i-2 ;
								if( prevNo < 0 ){
									i = videoSources.length - 1;
								}else{
									i = prevNo;
								}
								$('#videoPlayer').get(0).src = videoSources[i++ % videoSources.length];
								$('#videoPlayer').get(0).load();
								$('#videoPlayer').get(0).play();
							}
							// END video Preview

							// Start Left Image Fucntions
							function leftImage(){
								$(".currentTheme #leftImage .card-image").each(function() {  
									imgsrc = this.src;
										// ADD to array
										leftimageSources.push( imgsrc );
									});  

								$('#previewleftImage img').attr('src', leftimageSources[x++ % leftimageSources.length]);

								leftimageSourcesInterval = setInterval( function() {
										// READ from array
										var src = leftimageSources[x++ % leftimageSources.length];
										$('#previewleftImage img').hide().attr('src', src).fadeIn();
										
									}, 5000 );
							}

							// Start Bottom Image Fucntions
							function bottomImage(){
								$(".currentTheme #bottomImage .card-image").each(function() {  
									bottomimgsrc = this.src;
										// ADD to array
										bottomimageSources.push( bottomimgsrc );
									});  
								$('#previewbottomImage img').attr('src', bottomimageSources[bottomCounter++ % bottomimageSources.length]);

								bottomimageSourcesInterval = setInterval( function() {
										// READ from array
										var bottomsrc = bottomimageSources[bottomCounter++ % bottomimageSources.length];
										$('#previewbottomImage img').hide().attr('src', bottomsrc).fadeIn();
										
									}, 5000 );
							}

							// Start Top Image Fucntions
							function topImage(){
								$(".currentTheme #topImage .card-image").each(function() {  
									bottomimgsrc = this.src;
										// ADD to array
										topImageSources.push( bottomimgsrc );
									});  
								$('#previewtopImage .img1').attr('src', topImageSources[topCounter++ % topImageSources.length]);
								$('#previewtopImage .img2').attr('src', topImageSources[topCounter++ % topImageSources.length]);
								$('#previewtopImage .img3').attr('src', topImageSources[topCounter++ % topImageSources.length]);
								$('#previewtopImage .img4').attr('src', topImageSources[topCounter++ % topImageSources.length]);
								$('#previewtopImage .img5').attr('src', topImageSources[topCounter++ % topImageSources.length]);

								topimageSourcesInterval = setInterval( function() {
										// READ from array
										$('#previewtopImage .img1').hide().attr('src', topImageSources[topCounter++ % topImageSources.length]).fadeIn();
										$('#previewtopImage .img2').hide().attr('src', topImageSources[topCounter++ % topImageSources.length]).fadeIn();
										$('#previewtopImage .img3').hide().attr('src', topImageSources[topCounter++ % topImageSources.length]).fadeIn();
										$('#previewtopImage .img4').hide().attr('src', topImageSources[topCounter++ % topImageSources.length]).fadeIn();
										$('#previewtopImage .img5').hide().attr('src', topImageSources[topCounter++ % topImageSources.length]).fadeIn();
									}, 5000 );
							}

							// Start right forest mountain Image Fucntions
							function forestMountainImage(){
								$(".currentTheme #themeleftImage .card-image").each(function() {  
									imgsrc = this.src;
										// ADD to array
										leftimageSources.push( imgsrc );
									});  


								$('#mountainForestImage img').attr('src', leftimageSources[x++ % leftimageSources.length]);

								leftimageSourcesInterval = setInterval( function() {
										// READ from array
										$('#mountainForestImage img').hide().attr('src', leftimageSources[x++ % leftimageSources.length]).fadeIn();
									}, 5000 );
							}


							//  START sea & wind Theme Functions


							// Start sea & wind Theme  Right top Image Fucntions
							function swleftImage(){
								$(".currentTheme #rightImage .card-image").each(function() {  
									imgsrc = this.src;
										// ADD to array
										leftimageSources.push( imgsrc );
									});  

								$('#seaWindImage .img-top').attr('src', leftimageSources[x++ % leftimageSources.length]);

								leftimageSourcesInterval = setInterval( function() {
										// READ from array
										var src = leftimageSources[x++ % leftimageSources.length];
										$('#seaWindImage .img-top').hide().attr('src', src).fadeIn();
										
									}, 5000 );
							}

							// Start sea & wind Theme  Right middle Image Fucntions
							function swbottomImage(){
								$(".currentTheme #rightMiddle .card-image").each(function() {  
									bottomimgsrc = this.src;
										// ADD to array
										bottomimageSources.push( bottomimgsrc );
									});  
								
								$('#seaWindImage .img-middle').attr('src', bottomimageSources[bottomCounter++ % bottomimageSources.length]);

								bottomimageSourcesInterval = setInterval( function() {
										// READ from array
										var bottomsrc = bottomimageSources[bottomCounter++ % bottomimageSources.length];
										$('#seaWindImage .img-middle').hide().attr('src', bottomsrc).fadeIn();
										
									}, 5000 );
							}

							// Start sea & wind Theme Right bottom Image Fucntions
							function swtopImage(){
								$(".currentTheme #rightBottom .card-image").each(function() {  
									bottomimgsrc = this.src;
										// ADD to array
										topImageSources.push( bottomimgsrc );
									});  

								$('#seaWindImage .img-bottom').attr('src', topImageSources[topCounter++ % topImageSources.length]);

								topimageSourcesInterval = setInterval( function() {
										// READ from array
										$('#seaWindImage .img-bottom').hide().attr('src', topImageSources[topCounter++ % topImageSources.length]).fadeIn();
									}, 5000 );
							}
