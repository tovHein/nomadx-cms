---
##VERSION 0.3
1. [Add] Preview Function in Schedule Page.
1.1 [Add] schedule.js file for schedule page.
2. [Add] Device Page.
3. [CHANGED] php to html.
4. [CHANGED] re-structure for files.
5. [CREATE] mobileresponsive.scss in scss folder.
6. [UPDATE] main.js

---
##VERSION 0.2.1
1. [UPDATE] Schedule can accept Video and Images which allow to control duration. Default time is 10s.


AUTHOUR - HEIN (elTOV Singapore).
DATE - 15th January 2019.

---
##VERSION 0.2

Create Page
1. [CREATE] "User Register" Page (register.php).
2. [CREATE] "Schedule" Page (schedule.php).
3. [CREATE] "schedule.js" file for "Schedule Page".

Edit Page
1. Minor Update in (content.php).
2. Major Upade in ("eltovsg.css").
2. Major Upade in ("main.js").

Move Files
1. ("./css/scss/eltovsg.css") to ("./css/eltovsg.css").

AUTHOUR - HEIN (elTOV Singapore).
DATE - 10th January 2019.

---
##VERSION 0.1

Create Page
2. [CREATE] Product/Content Uploads Page.
1. [CREATE] Mobile Responsive Layout.

AUTHOUR - HEIN (elTOV Singapore).
DATE - 27th December 2018.

---
##DEMO URL
(http://128.199.176.133/nomadx_cms/contents.php).
**This is pure HTML PAGE**